%
% // Převzato z úkázkového kódu
%


main :- identify.

identify:-
  retractall(known(_,_,_)),         % clear stored information
  savec(X),
  write('Hledany savec je: '),write(X),nl.
identify:-
  write('Nemohu identifikovat savce.'),nl.
  
%
% // END
%  
  
  
savec(zirafa_rothschildova):-
	poddruh(sudo_dlouhokrci),
	hrb(nema),
	skvrny(pravidelne).
savec(zirafa_masajska):-
	poddruh(sudo_dlouhokrci),
	hrb(nema),
	skvrny(nepravidelne).
savec(velbloud_jednohrby):-
	poddruh(sudo_dlouhokrci),
	hrb(ma_jeden).
savec(velbloud_dvouhrby):-
	poddruh(sudo_dlouhokrci),
	hrb(ma_dva).
savec(prase_divoke):-
	poddruh(sudo_kratkokrci),
	cumak(rypak).
savec(kancil_mensi):-
	poddruh(sudo_kratkokrci),
	\+cumak(rypak).
savec(tapir_jihoamericky):-
	poddruh(licho_kratkokrci),
	\+roh(roste).
savec(nosorozec_tuponosy):-
	poddruh(licho_kratkokrci),
	roh(roste).
savec(kun_prevalsky):-
	poddruh(licho_dlouhokrci).
savec(kalon_egyptsky):-
	poddruh(letavi),
	bylozravec(je),
	echolokaci(ma).
savec(kalon_dlouhonosy):-
	poddruh(letavi),
	bylozravec(je),
	\+echolokaci(ma).
savec(tadarida_guanova):-
	poddruh(letavi),
	\+bylozravec(je),
	echolokaci(ma).
savec(jezek_usaty):-
	poddruh(neletavi),
	bodliny(ma).
savec(slon_africky):-
	poddruh(neletavi),
	\+bodliny(ma),
	kly(ma).
savec(mysice_krovinna):-
	poddruh(neletavi),
	\+bodliny(ma),
	\+kly(ma).
savec(klokan_hbity):-
	druh(vacnatci),
	ploskonozec(je).
savec(koala_medvidkovity):-
	druh(vacnatci),
	\+ploskonozec(je),
	bylozravec(je).
savec(vacice):-
	druh(vacnatci),
	\+ploskonozec(je),
	\+bylozravec(je),
	\+umi(letat).
savec(vakoveverka_letava):-
	druh(vacnatci),
	\+ploskonozec(je),
	umi(letat).
savec(jezura_australska):-
	druh(vejcorody),
	v_australii(zije).
savec(pajezura_bartonova):-
	druh(vejcorody),
	\+v_australii(zije).
savec(ptakopysk_podivny):-
	druh(vodnitvor),
	\+zivorodi.
savec(keporkak):-
	druh(vodnitvor),
	sudokopytnik(je),
	\+zuby(ma).
savec(delfin_pobrezni):-
	druh(vodnitvor),
	sudokopytnik(je),
	zuby(ma).
savec(dugong_indincky):-
	poddruh(ploutvonozec),
	bylozravec(je).
savec(tulen_obecny):-
	poddruh(ploutvonozec),
	\+bylozravec(je),
	\+kly(ma).
savec(mroz_ledni):-
	poddruh(ploutvonozec),
	\+bylozravec(je),
	kly(ma).

druh(kopytnici):-
	\+zije(ve_vode),
	zivorodi,
	vyvojPlacenta,
	kopyta(ma).
druh(nekopytnici):-
	\+zije(ve_vode),
	zivorodi,
	vyvojPlacenta,
	\+kopyta(ma).
druh(vacnatci):-
	\+zije(ve_vode),
	zivorodi,
	\+vyvojPlacenta.
druh(vejcorody):-
	\+zije(ve_vode),
	\+zivorodi.
druh(vodnitvor):-
	zije(ve_vode).


poddruh(sudo_kratkokrci):-
	druh(kopytnici),
	sudokopytnik(je),
	krk(kratky).
poddruh(sudo_dlouhokrci):-
	druh(kopytnici),
	sudokopytnik(je),
	\+krk(kratky).
poddruh(licho_kratkokrci):-
	druh(kopytnici),
	\+sudokopytnik(je),
	krk(kratky).
poddruh(licho_dlouhokrci):-
	druh(kopytnici),
	\+sudokopytnik(je),
	\+krk(kratky).
poddruh(letavi):-
	druh(nekopytnici),
	umi(letat).
poddruh(neletavi):-
	druh(nekopytnici),
	\+umi(letat).
poddruh(ploutvonozec):-
	druh(vodnitvor),
	\+sudokopytnik(je).
	
zivorodi :- ask('vyviji se mlade v tele matky','').
vyvojPlacenta :- ask('vyviji se mlade v placente','').
je(X):- ask(je,X).  
zije(X):- ask(zije,X).  
ma(X):- ask(ma,X).  
umi(X):- ask(umi,X).  
hrb(X):- menuask(hrb,X,[ma_jeden, ma_dva, nema]).
skvrny(X):- ask(skrvny,X).
cumak(X):- ask(je_cumak,X).
roh(X):- ask(X,roh).
bylozravec(X):- ask(X,bylozravec).
bodliny(X) :- ask(X,bodliny).
echolokaci(X) :- ask(X,echolokaci).
kly(X):- ask(X,kly).
kopyta(X) :- ask(X,kopyta).
v_australii(X) :- ask(X,v_australii).
sudokopytnik(X) :- ask(X,sudokopytnik).
ploskonozec(X) :- ask(X,ploskonozec).
krk(X):- ask(krk,X).
zuby(X) :- ask(X,zuby).


%
% // Převzato z úkázkového kódu
%

% 'ask' is responsible for getting information from the user, and remembering
% the users response. If it doesn't already know the answer to a question
% it will ask the user. It then asserts the answer. It recognizes two
% cases of knowledge: 1) the attribute-value is known to be true,
% 2) the attribute-value is known to be false.

% This means an attribute might have multiple values. A third test to
% see if the attribute has another value could be used to enforce
% single valued attributes. (This test is commented out below)

% For this system the menuask is used for attributes which are single
% valued

% 'ask' only deals with simple yes or no answers. a 'yes' is the only
% yes value. any other response is considered a 'no'.

ask(Attribute,Value):-
  known(yes,Attribute,Value),       % succeed if we know its true
  !.                                % and dont look any further
ask(Attribute,Value):-
  known(_,Attribute,Value),         % fail if we know its false
  !, fail.

ask(Attribute,_):-
  known(yes,Attribute,_),           % fail if we know its some other value.
  !, fail.                          % the cut in clause #1 ensures that if
                                    % we get here the value is wrong.
ask(A,V):-
  write(A:V),                       % if we get here, we need to ask.
  write('? (yes or no): '),
  read(Y),                          % get the answer
  asserta(known(Y,A,V)),            % remember it so we dont ask again.
  Y = yes.                          % succeed or fail based on answer.

% 'menuask' is like ask, only it gives the user a menu to to choose
% from rather than a yes on no answer. In this case there is no
% need to check for a negative since 'menuask' ensures there will
% be some positive answer.

menuask(Attribute,Value,_):-
  known(yes,Attribute,Value),       % succeed if we know
  !.
menuask(Attribute,_,_):-
  known(yes,Attribute,_),           % fail if its some other value
  !, fail.

menuask(Attribute,AskValue,Menu):-
  nl,write('What is the value for '),write(Attribute),write('?'),nl,
  display_menu(Menu),
  write('Enter the number of choice> '),
  read(Num),nl,
  pick_menu(Num,AnswerValue,Menu),
  asserta(known(yes,Attribute,AnswerValue)),
  AskValue = AnswerValue.           % succeed or fail based on answer

display_menu(Menu):-
  disp_menu(1,Menu), !.             % make sure we fail on backtracking

disp_menu(_,[]).
disp_menu(N,[Item | Rest]):-        % recursively write the head of
  write(N),write(' : '),write(Item),nl, % the list and disp_menu the tail
  NN is N + 1,
  disp_menu(NN,Rest).

pick_menu(N,Val,Menu):-
  integer(N),                       % make sure they gave a number
  pic_menu(1,N,Val,Menu), !.        % start at one
  pick_menu(Val,Val,_).             % if they didn't enter a number, use
                                    % what they entered as the value

pic_menu(_,_,none_of_the_above,[]). % if we've exhausted the list
pic_menu(N,N, Item, [Item|_]).      % the counter matches the number
pic_menu(Ctr,N, Val, [_|Rest]):-
  NextCtr is Ctr + 1,               % try the next one
  pic_menu(NextCtr, N, Val, Rest).
  
%
% // END
%