
% muzi

muz(lukas_barta).
muz(martin_barta).
muz(vladimir_barta).
muz(ales_novacek).
muz(vaclav_IV).
muz(petr_novacek).
muz(radek_novacek).
muz(frantisek_novacek).
muz(vit_novacek).
muz(jan_bilek).

% zeny

zena(sarka_bartova).
zena(irena_bartova).
zena(milena_bartova).
zena(marta_novackova).
zena(katerina_novackova).
zena(pavlina_novackova).
zena(milada_bilkova).

% vztah manzelstvi

manzelka(martin_barta,irena_bartova).
manzelka(milena_bartova,vladimir_barta).
manzelka(pavlina_novackova,radek_novacek).
manzelka(marta_novackova,frantisek_novacek).
manzelka(katerina_novackova,vit_novacek).
manzelka(milena_bartova,vladimir_barta).


% vztah otcovstvi

otec(martin_barta,lukas_barta).
otec(martin_barta,sarka_bartova).
otec(vladimir_barta,martin_barta).
otec(jan_bilek,milena_bartova).
otec(frantisek_novacek,irena_bartova).
otec(frantisek_novacek,radek_novacek).
otec(vit_novacek,frantisek_novacek).
otec(radek_novacek,ales_novacek).
otec(radek_novacek,petr_novacek).


% vztah materstvi

matka(irena_bartova,lukas_barta).
matka(irena_bartova,sarka_bartova).
matka(milena_bartova,martin_barta).
matka(milada_bilkova,milena_bartova).
matka(marta_novackova,irena_bartova).
matka(marta_novackova,radek_novacek).
matka(katerina_novackova,frantisek_novacek).
matka(pavlina_novackova,ales_novacek).
matka(pavlina_novackova,petr_novacek).



% vztahy definovane na zaklade predchozich pomoci pravidel

bratr(X,Y) :-
   muz(X),otec(O,X),matka(M,X),otec(O,Y),matka(M,Y).

sestra(X,Y) :-
   zena(X),otec(O,X),matka(M,X),otec(O,Y),matka(M,Y).

deda(X,Y) :-
   muz(X),otec(X,Z),(otec(Z,Y);matka(Z,Y)).

baba(X,Y) :-
   zena(X),matka(X,Z),(otec(Z,Y);matka(Z,Y)).

vnuk(X,Y) :-
   muz(X),(deda(Y,X);baba(Y,X)).

vnucka(X,Y) :-
   zena(X),(deda(Y,X);baba(Y,X)).

rodic(X,Y) :- otec(X,Y);matka(X,Y).

praded(X,Y) :- otec(X,Z),(deda(Z,Y);baba(Z,Y)).

prababa(X,Y) :- matka(X,Z),(deda(Z,Y);baba(Z,Y)).

