;K hod na kostce
;V hodnota po opakovanem hazeni sestkou
;G pomocna promenna na pocet hodu 6ky
(defglobal
?*G* = 0
?*K* = 0
?*V* = 0
?*T* = nil
?*house* = nil
?*token* = nil
?*empty* = nil
?*goal* = nil
)
;==========================================================================
;UVODNI OBRAZOVKY
;==========================================================================
(defrule starter 
		?init <- (initial-fact)
	=>
		(retract ?init)
		(printout t crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf)		
		(printout t "                                 	W E L C O M E   T O   L U D O" crlf)
		(printout t crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf)
		(printout t "Authors: Viktoria Benkova, Kevin Kroupa, Tomas Holas, Lukas Barta, Frantisek Kodidek" crlf)
		(printout t crlf)
		(printout t "Hit <cr> to begin our consulting" crlf)
		(bind ?answer (readline))
		(assert (screen first_scrn))
)

(defrule first_scrn 
              	?scrn <- (screen first_scrn)
	=>
		(retract ?scrn)
		(printout t crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf)		
              	(printout t "                                 	I will help you win the game of ludo." crlf)
		(printout t crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf)		    
              	(printout t "To begin the help, hit <cr> " crlf)
              	(bind ?answer (readline))
		(printout t crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf)		
              	(assert (roll roll_cub))
)
;==========================================================================
;HAZENI KOSTKOU
;==========================================================================
(defrule roll_cube 
		?roll <- (roll roll_cub)
	=>
		(retract ?roll)
		(printout t "                             	    Roll the cube for me please and insert number, you rolled." crlf)
   		(bind ?*K* (read))			
          	(assert (roll after_roll))						        	
)

;==========================================================================
;ROZHODOVANI O HODNOTE HODU
;==========================================================================
(defrule after_rolling1 "NENI 6"
		?roll <- (roll after_roll)
		(not (test (= ?*K* 6)))
	=>
		(retract ?roll)
		(assert (roll was_not_six))							        	
)

(defrule after_rolling2 "JE 6 A HODIL JSEM POPRVE"

		?roll <- (roll after_roll)
		(and  (test (= ?*K* 6)) (test (= ?*G* 0)) ) 
	=>
		(retract ?roll)
		(assert (roll was_six_and_first))						        	
)

(defrule after_rolling3 "6 A HAZIM PO NEKOLIKATE"

		?roll <- (roll after_roll)
		(and (test (= ?*K* 6)) (test (> ?*G* 0)))
	=>
		(retract ?roll)
		(assert (roll was_six_and_not_first))						        	
)

;==========================================================================
;HOZENA PRVNI 6KA
;==========================================================================
(defrule place_token 
		?roll <- (roll was_six_and_first)
	=>
		(retract ?roll)	
		(printout t "                             	    Do you have any token on board? (yes/no)"crlf)
		(bind ?*T* (read))	
		(bind ?*V* 0)	
		(assert (roll place))				        	
)
;==========================================================================
;HOZENA 6KA KTERA NEBYLA PRVNI (NEKOLIKATY HOD PO SOBE)
;==========================================================================
(defrule roll_again
		?roll <- (roll was_six_and_not_first)
	=>
		(retract ?roll)	
		(printout t "                             	    Roll again."crlf)
		(bind ?*G*(+ ?*G* 1))
		(bind ?*V* (+ ?*V* 6))
		(assert (roll roll_cub))						        	
)

;==========================================================================
;MA NASAZENO 0 FIGUREK A HODIL 6
;==========================================================================
(defrule place_token_on_board 
		?roll <- (roll place)
		(test (eq ?*T* no))
	=>
		(retract ?roll)
		(bind ?*T* yes)
		(printout t "                             	    Succesfully placed token on board.You can roll again." crlf)
		(bind ?*V* 0)	
		(assert (roll roll_cub))						        	
)
;==========================================================================
;MA NASAZENE NEJAKE FIGURKY, HAZI ZNOVA
;==========================================================================
(defrule roll_again_after_first_six
		?roll <- (roll place)
		(test (eq ?*T* yes))
	=>
		(retract ?roll)
		(printout t "                             	    You have already some token on board. You must roll again." crlf)
		(bind ?*V* (+ ?*V* 6))	
		(bind ?*G* (+ ?*G* 1))	
		(assert (roll roll_cub))						        	
)
;==========================================================================
;HOZENA JINA HODNOTA A NENI PO 6
;==========================================================================
(defrule not_six_first
		?roll <- (roll was_not_six)
		(test (= ?*G* 0))
	=>
		(retract ?roll)			
		(printout t "                            	     Move your token."crlf)
		(assert (roll move))						        	
)
;==========================================================================
;HOZENA JINA HODNOTA A JE PO 6
;==========================================================================
(defrule not_six_more
		?roll <- (roll was_not_six)
		(test (> ?*G* 0))
	=>
		(retract ?roll)		
		(bind ?*V* (+ ?*V* ?*K*))			
		(printout t "                             	    Move your token by: ")
		(printout t ?*V* crlf)
		(bind ?*V* 0)
		(bind ?*G* 0)	
		(assert (roll move))						        	
)
;==========================================================================
;HODNOTA NENI 6, POHYB PO DESCE
;==========================================================================
(defrule try_move
		?roll <- (roll move)		
	=>
		(retract ?roll)			
		(printout t "                             	    Is in final destination house?. (yes/no)"crlf)
		(bind ?*goal* (read))
		(assert (roll house))						        	
)
;==========================================================================
;CIL JE DOMECEK, ZJISITME, ZDA JE PRAZDNY NEBO NE
;==========================================================================
(defrule is_house
		?roll <- (roll house)
		(test (eq ?*goal* yes))		
	=>
		(retract ?roll)			
		(printout t "                             	    Ok, final destination is house, is it empty? (yes/no)"crlf)
		(bind ?*empty* (read))
		(assert (roll can_go_to_house))						        	
)
;==========================================================================
;CIL NENI DOMECEK, ZJISTIME, ZDA NEKOHO MUZEME VYHODIT
;==========================================================================
(defrule is_not_house
		?roll <- (roll house)
		(test (eq ?*goal* no))			
	=>
		(retract ?roll)			
		(printout t "                             	    Ok, final destination is not house, is there any other token? (yes/no)"crlf)
		(bind ?*token* (read))
		(assert (roll can_kick_token))						        	
)
;==========================================================================
;CIL NENI DOMECEK, MUZEME NEKOHO VYHODIT
;==========================================================================
(defrule can_kick
		?roll <- (roll can_kick_token)
		(test (eq ?*token* yes))			
	=>
		(retract ?roll)	
		(printout t "                             	    Kick that token."crlf)
		(assert (roll end))						        	
)
;==========================================================================
;CIL NENI DOMECEK, NEMUZEME NIKOHO VYHODIT POUZE POHYB
;==========================================================================
(defrule can_not_kick
		?roll <- (roll can_kick_token)
		(test (eq ?*token* no))			
	=>
		(retract ?roll)
		(printout t "                             	    You only moved."crlf)	
		(assert (roll end))						        	
)
;==========================================================================
;MUZEME STOUPNOUT NA DOMECEK
;==========================================================================
(defrule can_go_home
		?roll <- (roll can_go_to_house)
		(test (eq ?*empty* yes))			
	=>
		(retract ?roll)			
		(printout t "                             	    Nice, token is in house. Do you have any other token on starting place? (yes/no)"crlf)
		(bind ?*house* (read))
		(assert (roll try_end))						        	
)
;==========================================================================
;NEMUZEME STOUPNOUT NA DOMECEK
;==========================================================================
(defrule can_not_go_home
		?roll <- (roll can_go_to_house)
		(test (eq ?*empty* no))			
	=>
		(retract ?roll)			
		(printout t "                             	    Bad luck, try roll again next round."crlf)
		(assert (roll end))						        	
)
;==========================================================================
;NEBYLA POSLEDNI FIGURKA
;==========================================================================
(defrule is_not_last
		?roll <- (roll try_end)
		(test (eq ?*house* yes))			
	=>
		(retract ?roll)	
		(bind ?*T* 0)			
		(printout t "                             	    You have another token. Try roll 6 in next round."crlf)
		(assert (roll end))						        	
)
;==========================================================================
;BYLA POSLEDNI FIGURKA - KONEC HRY
;==========================================================================
(defrule is_last
		?roll <- (roll try_end)
		(test (eq ?*house* no))			
	=>
		(retract ?roll)			
		(printout t "                             	    Nice, you won the game!"crlf)
		(assert (roll end_game))						        	
)
;==========================================================================
;END TURN
;==========================================================================
(defrule end
		?roll <- (roll end)
	=>
		(retract ?roll)		
		(printout t crlf)	
		(printout t "                             	    END OF TURN."crlf crlf)
		(assert (roll roll_cub))						        	
)
;==========================================================================
;END GAME
;==========================================================================
(defrule end_game
		?roll <- (roll end_game)
	=>
		(retract ?roll)		
		(printout t crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf crlf)
		(printout t "...................................................................................."crlf)
		(printout t "...................................................................................."crlf)
		(printout t ".......................................YOU WON......................................"crlf)
		(printout t "...................................................................................."crlf)
		(printout t "...................................................................................."crlf)						        	
)
